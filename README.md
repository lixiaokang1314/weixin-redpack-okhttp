# weixin-redpack-okhttp

#### 介绍
微信公众号发送红包, 通过okhttp实现

#### 软件架构
无

#### 使用说明
需要在application.yml里面设置下面的参数

1.  openid: 微信公众号openId
2.  pay_mach_id: 微信商户号ID
3.  pay_key: 商户号key
4.  cert_path: 支付证书地址,支付证书在商户平台里面下载

- [其他参考](https://gitee.com/lixiaokang1314/weixin-redpack-okhttp/blob/master/HELP.md)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

