package com.example.weixinredpackokhttp.model;

import com.example.weixinredpackokhttp.annotation.XStreamCDATA;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author shadow
 */
@Data
@XStreamAlias("xml")
public class RedPackRespDto {

    @XStreamAlias("return_code")
    @XStreamCDATA
    private String return_code;
    @XStreamAlias("return_msg")
    @XStreamCDATA
    private String return_msg;

    @XStreamAlias("result_code")
    @XStreamCDATA
    private String result_code;
    @XStreamAlias("err_code")
    @XStreamCDATA
    private String err_code;
    @XStreamAlias("err_code_des")
    @XStreamCDATA
    private String err_code_des;

    @XStreamAlias("mch_billno")
    @XStreamCDATA
    private String mch_billno;
    @XStreamAlias("mch_id")
    @XStreamCDATA
    private String mch_id;
    @XStreamAlias("wxappid")
    @XStreamCDATA
    private String wxappid;
    @XStreamAlias("re_openid")
    @XStreamCDATA
    private String re_openid;
    @XStreamAlias("total_amount")
    @XStreamCDATA
    private Integer total_amount;
    @XStreamAlias("send_listid")
    @XStreamCDATA
    private String send_listid;

}
