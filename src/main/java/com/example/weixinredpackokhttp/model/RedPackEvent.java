package com.example.weixinredpackokhttp.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 红包事件
 * @author shadow
 */
@AllArgsConstructor
@Data
public class RedPackEvent {

    /**
     * 发送对象的openId
     */
    private String openId;

    /**
     * 发送红包的金额
     */
    private String money;

}
