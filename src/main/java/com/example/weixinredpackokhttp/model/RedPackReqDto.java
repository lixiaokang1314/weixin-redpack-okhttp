package com.example.weixinredpackokhttp.model;

import com.example.weixinredpackokhttp.annotation.XStreamCDATA;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 红包请求dto
 * @author shadow
 */
@Data
@XStreamAlias("xml")
public class RedPackReqDto {


    @XStreamAlias("nonce_str")
    @XStreamCDATA
    private String nonce_str;

    @XStreamAlias("sign")
    @XStreamCDATA
    private String sign;

    @XStreamAlias("mch_billno")
    @XStreamCDATA
    private String mch_billno;

    @XStreamAlias("mch_id")
    @XStreamCDATA
    private String mch_id;

    @XStreamAlias("wxappid")
    @XStreamCDATA
    private String wxappid;

    @XStreamAlias("send_name")
    @XStreamCDATA
    private String send_name;

    @XStreamAlias("re_openid")
    @XStreamCDATA
    private String re_openid;

    @XStreamAlias("total_amount")
    @XStreamCDATA
    private Integer total_amount;

    @XStreamAlias("total_num")
    @XStreamCDATA
    private Integer total_num;

    @XStreamAlias("wishing")
    @XStreamCDATA
    private String wishing;

    @XStreamAlias("client_ip")
    @XStreamCDATA
    private String client_ip;

    @XStreamAlias("act_name")
    @XStreamCDATA
    private String act_name;

    @XStreamAlias("remark")
    @XStreamCDATA
    private String remark;

    @XStreamAlias("scene_id")
    @XStreamCDATA
    private String scene_id;

    @XStreamAlias("risk_info")
    @XStreamCDATA
    private String risk_info;

}
