package com.example.weixinredpackokhttp.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.RandomUtil;
import com.example.weixinredpackokhttp.service.RedPackService;
import com.example.weixinredpackokhttp.utils.RedisKyes;
import com.example.weixinredpackokhttp.utils.RedisOperator;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author shadow
 */
@RestController
public class RedPackController {

    @Autowired
    private RedisOperator redisOperator;
    @Autowired
    private RedPackService redPackService;

    /**
     * 发送红包
     * @param openId 要发生对象的openID
     * @return
     */
    @GetMapping("/send")
    public String send(@RequestParam String openId) {
        Assert.notBlank(openId, "openId不能为空");
        return redPackService.send(openId);
    }

    /**
     * 加载红包
     * @return
     */
    @GetMapping("/load")
    public String load() {
        // 生成随机红包
        ArrayList<Double> redList = Lists.newArrayList();
        for (int i = 0; i < 100; i++) {
            redList.add(RandomUtil.randomDouble(1,200));
        }
        List<String> redStrList = redList.stream().map(Objects::toString).collect(Collectors.toList());
        // 放入Redis缓存
        redisOperator.lpushall(RedisKyes.RED_PACK_LIST, redStrList);
        return null;
    }

}
