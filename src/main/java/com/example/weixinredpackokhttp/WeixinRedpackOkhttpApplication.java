package com.example.weixinredpackokhttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shadow
 */
@SpringBootApplication
public class WeixinRedpackOkhttpApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeixinRedpackOkhttpApplication.class, args);
    }

}
