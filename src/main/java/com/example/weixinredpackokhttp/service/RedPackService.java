package com.example.weixinredpackokhttp.service;

/**
 * @author shadow
 */
public interface RedPackService {

    /**
     * 发送红包
     * @param openId 红包发送对象的openID
     * @return
     */
    String send(String openId);
}
