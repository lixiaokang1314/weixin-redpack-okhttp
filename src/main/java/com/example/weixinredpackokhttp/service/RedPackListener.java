package com.example.weixinredpackokhttp.service;

import cn.hutool.log.StaticLog;
import com.example.weixinredpackokhttp.model.RedPackEvent;
import com.example.weixinredpackokhttp.utils.EventBusUtil;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author shadow
 */
@Component
public class RedPackListener {

    @Autowired
    private RedPackSender redPackSender;

    @PostConstruct
    public void init() {
        EventBusUtil.register(this);
    }

    /**
     * 这里做了一个简单的转发, 其实 RedPackSender 里面的方法可以直接写在这里
     * @param event
     */
    @Subscribe
    public void onEvent(RedPackEvent event) {
        StaticLog.info("====>给[{}]发送红包[{}]元", event.getOpenId(), event.getMoney());
        redPackSender.sendRedPackByOk(event.getOpenId(), event.getMoney());
    }
}
