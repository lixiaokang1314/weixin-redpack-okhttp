package com.example.weixinredpackokhttp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

@Configuration
public class LuaConfiguration {

    /**
     * 初始化加载红包lua
     * @return Redis脚本对象
     */
    @Bean
    public DefaultRedisScript<String> redisScript() {
        ClassPathResource res = new ClassPathResource("red/red.lua");
        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(res));
        redisScript.setResultType(String.class);
        return redisScript;
    }
}
