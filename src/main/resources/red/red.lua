-- 抢红包脚本
--[[
--red:list 为 List 结构，存放预先生成的红包金额
red:draw_count:u:openid 为 k-v 结构，用户领取红包计数器
red:draw为 Hash 结构，存放红包领取记录
red:task 也为 List 结构，红包异步发放队列
openid 为用户的openid
return: 0: 用户已领取, -1: 红包以发完
]] --
local openid = KEYS[1]
local isDraw = redis.call("HEXISTS", "red:draw", openid)
-- 已经领取
if isDraw ~= 0 then
   return 0
end
local redMoney = redis.call("RPOP", "red:list")
-- 红包已发完,返回-1
if not redMoney then
    return -1
end
-- 领取人昵称为Fhb
local red = { money = redMoney, openid = KEYS[1] }
-- 领取记录
redis.call("HSET", "red:draw", openid, cjson.encode(red))
-- 处理队列
redis.call("RPUSH", "red:task", cjson.encode(red))

return cjson.encode(red)
