# Getting Started

### Guides

使用okhttp发送微信公众号红包

#### 文档
* [微信公众号红包官方文档](https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_1)
* [okhttp官方文档](https://square.github.io/okhttp/https/)
* [微信公众号](https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html)
* [推荐一个快速基础微信公众号的框架](https://gitee.com/binary/weixin-java-tools)
* [工具类集合Hutool](https://hutool.cn/docs/#/)

#### 说明

- 使用Redis调用lua脚本的原子性原理,来做并发控制
- 使用guava的EventBus来做消息通知
- 使用okhttp来调用微信接口

#### 简介
生成红包在项目里面是完全随机生成的,如果生产使用,可以考虑看看这篇文章

- [微信红包随机算法](https://cloud.tencent.com/developer/article/1699931)
